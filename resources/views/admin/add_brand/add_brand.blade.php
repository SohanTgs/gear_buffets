@extends('admin.master')

@section('title')
	Add brand
@endsection('title')

@section('body')
	
<style>
	.box{
		height:320px; 
		width:100%;
		background:;
		 

	}
	.name{
		text-align:center;
		font-style:italic;
	}
</style>

	<div class='main-content'>
<a href="{{ route('manage_brand') }}" class='btn btn-primary'>Go to brand list</a>	

<div class='box' align="center">

<h2 class='name'>Add brand</h2> <hr/>


<br/>

<form action="{{ route('save_brand') }}" method="POST">

@csrf

<table>
<tr valign="top">
<td><label for='brand_name'>Brand Name * </label></td> <td><input type='text' size='40' id='brand_name' name='brand_name'>{{ $errors->has('brand_name') ? $errors->first('brand_name'):'' }}</td>
</tr>

<tr valign="top">
<td><label for="brand_description">Brand Description * </label></td><td> <textarea name='brand_description' id='brand_description' rows='2' cols="38"></textarea>{{ $errors->has('brand_description') ? $errors->first('brand_description'):'' }}</td>
</tr>
<tr valign="top">
<td><label for='publication_status'>Publication Status * </label> </td> <td>Published <input type="radio" checked="" name="publication_status" value='1' /> |
					 Unublished <input type="radio" id='publication_status' name="publication_status" value='0'/>{{ $errors->has('publication_status') ? $errors->first('publication_status'):'' }}</td>

</tr>
<tr>
<td></td>	
	<td><input type='submit' value='Save brand info' name='btn' class='btn-success form-control'></td>
</tr>
</table>
<br/><br/>
<div style="text-align:center;font-style:italic;color:red;"><h2>
{{ Session::get('sms') }}
</h2></div>
</form>
</div>
	</div>

@endsection('body')