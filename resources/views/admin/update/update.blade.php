@extends('admin.master')

@section('title')
	Update category
@endsection('title')

@section('body')

	<style>
	.box{
		height:320px; 
		width:100%;
		background:;
		 

	}
	.name{
		text-align:center;
		font-style:italic;
	}
</style>

	<div class='main-content'>
<a href="{{ route('manage_category') }}" class='btn btn-primary'>Go to category list</a>	

<div class='box' align="center">

<h2 class='name'>Update Category</h2> <hr/>


<br/>

<form action="{{ route('update_save') }}" method="POST">

@csrf


<table>
<tr valign="top">
<td><label for='category_name'>Category Name:</label></td> <td><input type='text' id='category_name' value='{{ $category->category_name }}' size='40' required="" name='category_name'></td>
<input type='hidden' value='{{ $category->id }}' name='category_id'>
</tr>

<tr valign="top">
<td><label for='category_description'>Category Description:</label></td><td> <textarea name='category_description' id='category_description' required="" rows='2' cols="38">{{ $category->category_description }}</textarea></td>
</tr>
<tr valign="top">
<td><label for='publication_status'>Publication Status :</label> </td> <td>Published <input type="radio" name="publication_status" {{ $category->publication_status == 1 ? 'checked':'' }} value='1' /> |
					 Unublished <input type="radio" id='publication_status' name="publication_status" {{ $category->publication_status == 0 ? 'checked':'' }} value='0'/></td>

</tr>
<tr>
<td></td>	
	<td><input type='submit' value='Update category info' name='btn' class='btn-success form-control'></td>
</tr>
</table>
<br/><br/>

</form>
</div>
	</div>

@endsection('body')