@extends('admin.master')

@section('title')
	Update brand
@endsection('title')

@section('body')
	
<style>
	.box{
		height:320px; 
		width:100%;
		background:;
		 

	}
	.name{
		text-align:center;
		font-style:italic;
	}
</style>

	<div class='main-content'>

<a href="{{ route('manage_brand') }}" class='btn btn-primary'>Go to brand list</a>	

<div class='box' align="center">

<h2 class='name'>Update brand</h2> <hr/>


<br/>

<form action="{{ route('brandUpdate_save') }}" method="POST">

@csrf

<table>
<tr valign="top">
<td><label for='brand_name'>Brand Name:</label></td> <td><input type='text' id='brand_name' size='40' value='{{ $brand->brand_name }}' name='brand_name'></td>
<input type='hidden' value='{{ $brand->id }}' name='brand_id'>
</tr>

<tr valign="top">
<td><label for='brand_description'>Brand Description:</label></td><td> <textarea name='brand_description' id='brand_description' rows='2' cols="38">{{ $brand->brand_description}}</textarea></td>
</tr>
<tr valign="top">
<td><label for='publication_status'>Publication Status :</label> </td> <td>Published <input type="radio" {{ $brand->publication_status == 1 ? 'checked' : '' }} name="publication_status" value='1' /> |
					 Unublished <input type="radio" id='publication_status' {{ $brand->publication_status == 0 ? 'checked' : '' }} name="publication_status" value='0'/></td>

</tr>
<tr>
<td></td>	
	<td><input type='submit' value='Save brand info' name='btn' class='btn-success form-control'></td>
</tr>
</table>
<br/><br/>


</form>
</div>
	</div>

@endsection('body')

