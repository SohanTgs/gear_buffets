@extends('admin.master')

@section('title')
	Manage product
@endsection('title')

@section('body')  

	<div class='main-content'>
	
<a href="{{ route('add_product') }}" class='btn btn-primary'>Add</a>

	<div class="box" align="center"><h2><i>Manage product</i></h2></div><hr/>
	<div style="text-align:center;font-style:italic;color:red;"><h2>
	{{ Session::get('sms') }} {{ Session::get('sms2') }} {{ Session::get('message') }} 
	<h2/></div>

	<br/>
	<h6 align="center">       </h6>
		<table class='table table-bordered table-active table-hover'>
			<tr align="center">
				<th>SI No</th>              
				<th>Category name</th>
				<th>Brand name</th>
				<th>Product name</th>
				<th>Product image</th>
				<th>Product price</th>
				<th>Product quantity</th>
				<th>Publication status</th>
				<th>Action</th>
			</tr>
		    @php($i=1)
		    @foreach($products as $product)
			<tr>
				<td align="center">{{ $i++ }}</td>
				<td align="center">{{ $product->category_name }}</td>
				<td align="center">{{ $product->brand_name }}</td>
				<td align="center">{{ $product->product_name }}</td>
				<td align="center"><img src= "{{ asset($product->product_image) }}" height="100"> </td>
				<td align="center">{{ $product->product_price }}</td>
				<td align="center">{{ $product->product_quantity }}</td>
				@if($product->publication_status == 1)
				<td align="center">Published</td>
				@else
				<td align="center">Unpublished</td>
				@endif
				<td align="center">
					<a href="{{ route('product_update',['id'=>$product->id]) }}">Update</a> <br/>
					<a href="{{ route('product_delete',['id'=>$product->id]) }}" onclick="return confirm('Are you sure?')" >Delete</a><br/>
				@if($product->publication_status == 1)
					       <a href="{{ route('product_unpublished',['id'=>$product->id]) }}">Unpublished</a>
				@else	
							<a href="{{ route('product_published',['id'=>$product->id]) }}">Published</a>       
				@endif
				</td>
			</tr>
			@endforeach
		</table>
<div class='d-flex justify-content-center'>	{{ $products->links() }} </div>
	</div>

@endsection('body')