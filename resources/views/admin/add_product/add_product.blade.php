@extends('admin.master')

@section('title')
	Add product
@endsection('title')

@section('body')

<style>
	.box{
		height:320px; 
		width:100%;
		background:;
		 

	}
	.name{
		text-align:center;
		font-style:italic;
	}
	.main-content{
		height: 800px;
	}
</style>

	<div class='main-content'>
	
<a href="{{ route('manage_product') }}" class='btn btn-primary'>Go to product list</a>

<div class='box' align="center">

<h2 class='name'>Add product</h2> <hr/>

<div style="text-align:center;font-style:italic;color:red;"><h2>
 {{ Session::get('message') }}
</h2></div>

<br/>

<form action="{{ route('new_product') }}" method="POST" enctype="multipart/form-data">

@csrf


<table>
<tr valign="top">
	<td  height="40">Category name * </td>
<td> 
	<select name='category_id'>
		<option value=''>---Select category name---</option>
		@foreach($categories as $category)
		<option value='{{ $category->id }}'>{{ $category->category_name }}</option>
		@endforeach
</select> {{ $errors->has('category_id') ? $errors->first('category_id'):'' }}</td>
</tr>

<tr valign="top">
<td height="40">Brand name * </td><td>
	<select name='brand_id'>
	    <option value="">---Select category name---</option>
	    @foreach($brands as $brand)
	    <option value="{{ $brand->id }}">{{ $brand->brand_name }}</option>
	    @endforeach
	</select>
{{ $errors->has('brand_id') ? $errors->first('brand_id'):'' }}</td>
</tr>
<tr>
  <td height="40"><label for='product_name'>Product name * </label></td>
  <td><input type='text' name='product_name' id='product_name' size='40'>{{ $errors->has('product_name') ? $errors->first('product_name'):'' }} </td>
</tr>
<tr>
  <td height="40"><label for='product_price'>Product price * </label></td>
  <td><input type='number' name='product_price' id='product_price'>{{ $errors->has('product_price') ? $errors->first('product_price'):'' }} </td>
</tr>
<tr>
  <td height="40"><label for='product_quantity'>Product quantity * </label></td>
	<td><input type="number" name='product_quantity' id='product_quantity'>{{ $errors->has('product_quantity') ? $errors->first('product_quantity'):'' }}</td>
</tr>
<tr>
	<td height="40"><label for='short_description'>Short description * </label></td>
  <td><textarea name='short_description' id='short_description' cols='39'></textarea>{{ $errors->has('short_description') ? $errors->first('short_description'):'' }}</td>	
</tr>
<tr>
	<td height="80"><label for='long_description'>Long description * </label></td>
  <td><textarea name='long_description' id='long_description' cols='39' rows="3"></textarea>{{ $errors->has('long_description') ? $errors->first('long_description'):'' }}</td>	
</tr>
<tr>
  <td height="40"><label for='product_image'>Product image * </label></td>
	<td><input type="file" name='product_image' id='product_image' accept="image/*">{{ $errors->has('product_image') ? $errors->first('product_image'):'' }}</td>
</tr>
<tr valign="top">
<td height="40"><label for='publication_status'>Publication <br/> Status * </label> 
</td> 
<td>Published <input type="radio" name="publication_status" checked value='1'/> |
   Unublished <input type="radio" name="publication_status" id='publication_status' value='0'/>
{{ $errors->has('publication_status') ? $errors->first('publication_status'):'' }}</td>

</tr>
<tr>
<td></td>	
	<td><input type='submit' value='Save product info' name='btn' class='btn-success form-control'></td>
</tr>
</table>
<br/><br/>


</form>
</div>
	</div>

@endsection('body')