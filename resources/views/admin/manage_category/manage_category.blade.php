@extends('admin.master')

@section('title')
	Manage category
@endsection('title')

@section('body')

	<div class='main-content'>

<a href="{{ route('add_cetegory') }}" class='btn btn-primary'>Add</a>

	<div class="box" align="center"><h2><i>Manage category</i></h2></div>
<hr/>	

<div style="text-align:center;font-style:italic;color:red;"><h2>

{{ Session::get('sms') }}{{ Session::get('sms2') }}{{ Session::get('message') }}{{ Session::get('message3') }}

<h2/></div>

	
		<table class='table table-bordered table-active table-hover'>
			<tr align="center">
				<th>SI No</th>              
				<th>Category name</th>
				<th>Category description</th>
				<th>Publication status</th>
				<th>Action</th>
			</tr>
			@php($i=1)
			@foreach($categories as $category)
			<tr>
				<td align="center">{{ $i++ }}</td>
				<td>{{ $category->category_name }}</td>
				<td>{{ $category->category_description }}</td>
				<td align="center">{{ $category->publication_status == 1 ? 'Published':'Unpublished' }}</td>
				<td align="center">
					<a href="{{ route('update_category',['id'=>$category->id])}}">Update</a> |
					<a href="{{ route('delete_category',['id'=>$category->id])}}" onclick="return confirm('Are you sure?')" >Delete</a><br/>
					@if($category->publication_status == 1)
					<a href="{{ route('Unpublished_category',['id'=>$category->id]) }}">Unpublished</a>
					@else
					<a href="{{ route('published_category',['id'=>$category->id]) }}">Published</a>
					@endif
				</td>
			</tr>
			@endforeach
		</table>


	<div class='d-flex justify-content-center'>	{{ $categories->links() }} </div>

	</div>

@endsection('body')