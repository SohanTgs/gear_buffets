@extends('admin.master')

@section('title')
	Mangage brand
@endsection('title')

@section('body')
	
<div class='main-content'>

<a href="{{ route('add_brand') }}" class='btn btn-primary'>Add</a>	

	<div class="box" align="center"><h2><i>Manage brand</i></h2></div><hr/>
	<div style="text-align:center;font-style:italic;color:red;"><h2>
	 {{ Session::get('sms') }}  {{ Session::get('sms2') }}  {{ Session::get('sms3') }} {{Session::get('message')}} 
	 <h2/></div>
		<table class='table table-bordered table-active table-hover'>
			<tr align="center">
				<th>SI No</th>              
				<th>Brand name</th>
				<th>Brand description</th>
				<th>Publication status</th>
				<th>Action</th>
			</tr>
			@php($i=1)
			@foreach($brands as $brand)
			<tr>
				<td align="center">{{ $i++ }}</td>
				<td>{{ $brand->brand_name }}</td>
				<td>{{ $brand->brand_description }}</td>
				<td align="center">{{ $brand->publication_status == 1 ? 'Pushlished':'Unpublished'}}</td>
				<td align="center">
					<a href="{{ route('update_brand',['id'=>$brand->id]) }}">Update</a> |
					<a href="{{ route('brand_delete',['id'=>$brand->id]) }}" onclick="return confirm('Are you sure?')" >Delete</a><br/>
				@if($brand->publication_status == 1)	
					<a href="{{ route('Unpublished_brand',['id'=>$brand->id]) }}">Unpublished</a>
				@else	
					 <a href="{{ route('Published_brand',['id'=>$brand->id]) }}">Published</a> 
				@endif	
				</td>
			</tr>
			@endforeach
		</table>
       <div class='d-flex justify-content-center'> {{ $brands->links() }} </div>  

@endsection('body')