@extends('admin.master')

@section('title')
	Add category
@endsection('title')

@section('body')
	
<style>
	.box{
		height:320px; 
		width:100%;
		background:;
		 

	}
	.name{
		text-align:center;
		font-style:italic;
	}
</style>

	<div class='main-content'>
	
<a href="{{ route('manage_category') }}" class='btn btn-primary'>Go to category list</a>

<div class='box' align="center">

<h2 class='name'>Add Category</h2> <hr/>


<br/>

<form action='{{ route('category_save') }}' method="POST">

@csrf


<table>
<tr valign="top">
<td><label for='category_name'>Category Name * </label></td> <td><input type='text' size='40'  name='category_name' id='category_name'>{{ $errors->has('category_name') ? $errors->first('category_name'):'' }}</td>
</tr>

<tr valign="top">
<td><label for='category_description'>Category Description * </label></td><td> <textarea name='category_description' id='category_description'  rows='2' cols="38"></textarea>{{ $errors->has('category_description') ? $errors->first('category_description'):'' }}</td>
</tr>
<tr valign="top">
<td><label for='publication_status'>Publication Status * </label> </td> <td>Published <input type="radio" checked="" name="publication_status" value='1'/> |
					 Unublished <input type="radio" id='publication_status' name="publication_status" value='0'/>{{ $errors->has('publication_status') ? $errors->first('publication_status'):'' }}</td>

</tr>
<tr>
<td></td>	
	<td><input type='submit' value='Save category info' name='btn' class='btn-success form-control'></td>
</tr>
</table>
<br/><br/>
<div style="text-align:center;font-style:italic;color:red;"><h2>
{{ Session::get('message') }}
</h2></div>
</form>
</div>
	</div>

@endsection('body')