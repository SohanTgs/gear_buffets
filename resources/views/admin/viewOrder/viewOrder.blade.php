@extends('admin.master')

@section('title')
	View order
@endsection('title')

@section('body')

	<div class='main-content'>

<a href="{{ route('manageOrder') }}" class='btn btn-primary'>Go to order list</a>
		
<table class="table table-bordered table-striped table-dark table-hover" id='result'>
		<h2 align="center">Order details info for this order</h2>
			<tr align="center">	
				<th align="center">Order No</th>	
				<td align="center">{{ $order->id }}</td>
			</tr>
			<tr align="center">	
				<th align="center">Order total</th>	
				<td align="center">{{ $order->order_total }}</td>
			</tr>
			<tr align="center">	
				<th align="center">Order date</th>	
				<td align="center">{{ $order->created_at }}</td>	
			</tr>
			<tr align="center">	
				<th align="center">Order staus</th>	
				<td align="center">{{ $order->order_status }}</td>	
			</tr>										
		</table>
		<hr/>
<table class="table table-bordered table-striped table-dark table-hover" id='result'>
		<h2 align="center">Customer info for this order</h2>
			<tr align="center">	
				<th align="center">Customer name</th>	
				<td align="center">{{ $customer->name }}</td>
			</tr>
			<tr align="center">	
				<th align="center">Phone number</th>	
				<td align="center">{{ $customer->number }}</td>
			</tr>
			<tr align="center">	
				<th align="center">Email</th>	
				<td align="center">{{ $customer->email }}</td>	
			</tr>
			<tr align="center">	
				<th align="center">Address</th>	
				<td align="center">{{ $customer->address }}</td>	
			</tr>										
		</table>
		<hr/>
<table class="table table-bordered table-striped table-dark table-hover" id='result'>
		<h2 align="center">Shipping info for this order</h2>
			<tr align="center">	
				<th align="center">Name</th>	
				<td align="center">{{ $shipping->name }}</td>
			</tr>
			<tr align="center">	
				<th align="center">Phone number</th>	
				<td align="center">{{ $shipping->number }}</td>
			</tr>
			<tr align="center">	
				<th align="center">Email</th>	
				<td align="center">{{ $shipping->email }}</td>	
			</tr>
			<tr align="center">	
				<th align="center">Address</th>	
				<td align="center">{{ $shipping->address }}</td>	
			</tr>										
		</table>
<hr/>
<table class="table table-bordered table-striped table-dark table-hover" id='result'>
		<h2 align="center">Payment info for this order</h2>
			<tr align="center">	
				<th align="center">Payment type</th>	
				<td align="center">{{ $payment->payment_type }}</td>
			</tr>
			<tr align="center">	
				<th align="center">Payment status</th>	
				<td align="center">{{ $payment->payment_status }}</td>
			</tr>										
		</table>
		<hr/>
		<table class='table table-bordered table-active table-hover'>
			<tr align="center">
				<th>SI No</th>              
				<th>Product Id</th>
				<th>Product name</th>
				<th>Product price</th>
				<th>Product quantity</th>
				<th>Total price</th>
			</tr>
			@php($i=1)
			@foreach($orderDetail as $details)
			<tr>
				<td align="center">{{ $i++ }}</td>
				<td align="center">{{ $details->product_id }}</td>
				<td align="center">{{ $details->product_name }}</td>
				<td align="center">{{ $details->product_price }}</td>
				<td align="center">{{ $details->product_quantity }}</td>
				<td align="center">{{ $details->product_quantity*$details->product_price }}</td>
				<td align="center"></td>
			</tr>
			@endforeach
		</table>
<a href="{{ route('manageOrder') }}" class='btn btn-primary'>Go to order list</a>
	</div>	


@endsection('body')