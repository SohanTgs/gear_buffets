@extends('admin.master')

@section('title')
	Manage order
@endsection('title')

@section('body')
	
	<div class='main-content'>

	

	<div class="box" align="center"><h2><i>Manage orders</i></h2></div><hr/>

	<div style="text-align:center;font-style:italic;color:red;"><h2>
{{ Session::get('message') }} {{ Session::get('sms') }}
	 <h2/></div>
		<table class='table table-bordered table-active table-hover'>
			<tr align="center">
				<th>SI No</th>              
				<th>Customer name</th>
				<th>Order total</th>
				<th>Order date</th>
				<th>Order status</th>
				<th>Payment type</th>
				<th>Payment status</th>
				<th>Action</th>
			</tr>
			@php($i=1)
			@foreach($orders as $order)
			<tr>
				<td align="center">{{ $i++ }}</td>
				<td>{{ $order->name }}</td>
				<td>{{ $order->order_total }}</td>
				<td>{{ $order->created_at }}</td>
				<td>{{ $order->order_status }}</td>
				<td>{{ $order->payment_type }}</td>
				<td align="center">{{ $order->payment_status }}</td>
				
				<td align="center">
					
					<a href="{{ route('viewOrder',['id'=>$order->id]) }}" class="btn btn-success btn-xs">View</a><br/> 
					<a href="{{ route('invoice',['id'=>$order->id]) }}">Invoice</a><br/>
					<a href="{{ route('pdf',['id'=>$order->id]) }}">PDF</a><br/>
					@if($order->order_status == 'Pending')
					<a href="{{ route('compelteStatus',['id'=>$order->id]) }}" onclick="return confirm('Are you sure?')">Complete</a><br/> 
					@else
					Succesfull <br/> 
					@endif
					<a href="{{ route('removeOrder',['id'=>$order->id]) }}" onclick="return confirm('Are you sure?')" >Delete</a><br/>

				</td>

			</tr>
			@endforeach
		</table>
       

@endsection('body')