@extends('admin.master')

@section('title')
	Add product
@endsection('title')

@section('body')

<style>
	.box{
		height:320px; 
		width:100%;
		background:;
		 

	}
	.name{
		text-align:center;
		font-style:italic;
	}
	.main-content{
		height: 870px;
	}
</style>

	<div class='main-content'>
	
<a href="{{ route('manage_product') }}" class='btn btn-primary'>Go to product list</a>

<div class='box' align="center">

<h2 class='name'>Update product</h2>           <hr/>


<br/>

<form action="save_productUpdate" method="POST" enctype="multipart/form-data">

@csrf


<table>
<tr valign="top">
	<td  height="40">Category name:</td>
<td> 
	<select name='category_id'>		
		@foreach($categories as $category)
		@if($category->id == $products->category_id )
			<option value='{{ $category->id }}' selected>{{ $category->category_name }}</option>
		@else
			<option value='{{ $category->id }}'>{{ $category->category_name }}</option>
		@endif	
		@endforeach
</select> </td>
</tr>

<tr valign="top">
<td height="40">Brand name:</td><td>
	<select name='brand_id'>
	  
	    @foreach($brands as $brand)
		@if($brand->id == $products->brand_id)
		 <option value="{{ $brand->id }}" selected>{{ $brand->brand_name }}</option>
		@else 
		  <option value="{{ $brand->id }}">{{ $brand->brand_name }}</option>
		@endif  
		 @endforeach
	</select>
</td>
</tr>
<tr>
  <td height="40"><label for='product_name'>Product name:</label></td>
  <input type='hidden' name='product_id' size='40' value="{{ $products->id }}">
  <td><input type='text' name='product_name' id='product_name' size='40' value="{{ $products->product_name }}"> </td>
</tr>
<tr>
  <td height="40"><label for='product_price'>Product price:</label></td>
  <td><input type='number' name='product_price' id='product_price' value="{{ $products->product_price }}" > </td>
</tr>
<tr>
  <td height="40"><label for='product_quantity'>Product quantity:</label></td>
	<td><input type="number" name='product_quantity' id='product_quantity' value="{{ $products->product_quantity }}"></td>
</tr>
<tr>
	<td height="40"><label for='short_description'>Short description:</label></td>
  <td><textarea name='short_description' id='short_description' cols='39'>{{ $products->short_description }}</textarea></td>	
</tr>
<tr>
	<td height="80"><label for='long_description'>Long description:</label></td>
  <td><textarea name='long_description' id='long_description' cols='39' rows="3">{{ $products->long_description }}</textarea></td>	
</tr>
<tr>
  <td>
  	<img src="{{ asset($products->product_image) }}" height="200">
  </td>
	<td><input type="file" name='product_image' accept="image/*"></td>
</tr>
<tr valign="top">
<td height="40"><label for='publication_status'>Publication <br/> Status :</label> 
</td> 
<td>Published <input type="radio" name="publication_status" value='1' {{ $products->publication_status == 1 ? 'checked' : ''}}/> |
   Unublished <input type="radio" name="publication_status" id='publication_status' value='0' {{ $products->publication_status == 0 ? 'checked' : ''}}/>
</td>

</tr>
<tr>
<td></td>	
	<td><input type='submit' value='Update product info' name='btn' class='btn-success form-control'></td>
</tr>
</table>
<br/><br/>


</form>
</div>
	</div>

@endsection('body')