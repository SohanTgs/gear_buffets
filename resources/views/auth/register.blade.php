<!DOCTYPE html>
<html lang="en">


<!-- auth-register.html  21 Nov 2019 04:05:01 GMT -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Register</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('/') }}/admin/css/app.min.css">
  <link rel="stylesheet" href="{{ asset('/') }}/admin/bundles/jquery-selectric/selectric.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('/') }}/admin/css/style.css">
  <link rel="stylesheet" href="assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="{{ asset('/') }}/admin/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='{{ asset('/') }}/admin/img/favicon.ico' />
</head>

<body>
  <div class="loader"></div>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
            <div class="card card-primary">
              <div class="card-header">
                <h4>Register</h4>
              </div>
              <div class="card-body">
               

             <form method="POST" action="{{ route('register') }}">
                 
                 @csrf


                  <div class="row">
                    <div class="form-group col-8">
                      <label for="name">Full Name*</label>
                      <input id="frist_name" type="text" class="form-control" name="name" autofocus>
                    </div>
                   {{ $errors->has('name') ? $errors->first('name') : ' ' }}
                  </div>
                  <div class="form-group">
                    <label for="email">Email*</label>
                    <input id="email" type="email" class="form-control" name="email">
                    <div class="invalid-feedback">
                    </div>
                    {{ $errors->has('email') ? $errors->first('email') : ' ' }}
                  <span id="result"></span>
                  </div>
                  <div class="row">
                    <div class="form-group col-6">
                      <label for="password" class="d-block">Password*</label>
                      <input id="password" type="password" class="form-control pwstrength" data-indicator="pwindicator"
                        name="password">
                      <div id="pwindicator" class="pwindicator">
                        <div class="bar"></div>
                        <div class="label"></div>{{ $errors->has('password') ? $errors->first('password') : ' ' }}
                      </div>
                    </div>
                    <div class="form-group col-6">
                      <label for="password2" class="d-block">Password Confirmation*</label>
                      <input id="password2" required="" type="password" class="form-control" name="password_confirmation">
                    </div>
                  </div>

                   <div class="row">
                    <div class="form-group col-6">
                      <label for="Mobile" class="d-block">Mobile*</label>
                      <input type="number" id="Mobile" name='mobile' class="form-control pwstrength" data-indicator="pwindicator">
                      <div id="pwindicator" class="pwindicator">
                        <div class="bar"></div>
                        <div class="label"></div>
                      </div>{{ $errors->has('mobile') ? $errors->first('mobile') : ' ' }}
                    </div>
                    <div class="form-group col-6">
                      <label for="location" class="d-block">Location*</label>
                      <input type="text" id="location" name='location' class="form-control">
                {{ $errors->has('location') ? $errors->first('location') : ' ' }}
                  </div>

                    <div class="form-group col-6">
                      <label for="bio" class="d-block">Bio*</label>
                      <textarea name='bio' rows='4' cols="82" id='bio'></textarea>
                    </div>
                    </div>

{{ $errors->has('bio') ? $errors->first('bio') : ' ' }}

                  <div class="form-group">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" disabled name="agree" class="custom-control-input" id="agree">
                      <label class="custom-control-label" for="agree" >I agree with the terms and conditions</label>
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="submit" id='regBtn' class="btn btn-primary btn-lg btn-block">
                      Register
                    </button>
                  </div>


               
                </form>



              </div>
              <div class="mb-4 text-muted text-center">
                Already Registered? <a href="{{ url('/login') }}">Login</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- General JS Scripts -->
  <script src="{{ asset('/') }}/admin/js/app.min.js"></script>
  <!-- JS Libraies -->
  <script src="{{ asset('/') }}/admin/bundles/jquery-pwstrength/jquery.pwstrength.min.js"></script>
  <script src="{{ asset('/') }}/admin/bundles/jquery-selectric/jquery.selectric.min.js"></script>
  <!-- Page Specific JS File -->
  <script src="{{ asset('/') }}/admin/js/page/auth-register.js"></script>
  <!-- Template JS File -->
  <script src="{{ asset('/') }}/admin/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="{{ asset('/') }}/admin/js/custom.js"></script>

<script>

 var input = document.getElementById('email');
 input.oninput = function() {

    var xmlHttp = new XMLHttpRequest();
    var mail = document.getElementById('email').value;
    var serverPage = 'http://localhost/gear-buffets/public/adminLogin/'+mail;
    xmlHttp.open('GET',serverPage);
    xmlHttp.onreadystatechange = function() {
          
      if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
        document.getElementById('result').innerHTML = xmlHttp.responseText;
           if(xmlHttp.responseText == 'Already Exists'){
                document.getElementById('regBtn').disabled = true;
              
            }else{
                document.getElementById('regBtn').disabled = false;
               
            }
      }
    }
    xmlHttp.send(null);
  }
</script>




</body>


<!-- auth-register.html  21 Nov 2019 04:05:02 GMT -->
</html>