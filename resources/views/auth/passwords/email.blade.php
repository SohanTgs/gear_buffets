<!DOCTYPE html>
<html lang="en">


<!-- auth-forgot-password.html  21 Nov 2019 04:05:02 GMT -->
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Reset password</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{ asset('/') }}/admin/css/app.min.css">
  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('/') }}/admin/css/style.css">
  <link rel="stylesheet" href="{{ asset('/') }}/admin/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="{{ asset('/') }}/admin/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='{{ asset('/') }}/admin/img/favicon.ico' />
</head>

<style type="text/css">
    .card{
        height: 320px;
        width: 500px;
        margin-left: -50px; 
    }    
</style>

<body>
  <div class="loader"></div>
  <div id="app">
    <section class="section">
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="card card-primary">
              <div class="card-header">
                <h4>Forgot Password</h4>
              </div>
              <div class="card-body">
                <p class="text-muted">We will send a link to reset your password</p>
             


               <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>

    <br/>   

     @if (session('status'))
          <div class="alert alert-success" role="alert">
         {{ session('status') }}
          </div>
     @endif            


              </div>
            </div>
              Don't have an account? <a href="{{ url('register') }}">Register</a> <br/>
              Have an account? <a href="{{ url('login') }}">Login</a>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- General JS Scripts -->
  <script src="{{ asset('/') }}/admin/js/app.min.js"></script>
  <!-- JS Libraies -->
  <!-- Page Specific JS File -->
  <!-- Template JS File -->
  <script src="{{ asset('/') }}/admin/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="{{ asset('/') }}/admin/js/custom.js"></script>
</body>


<!-- auth-forgot-password.html  21 Nov 2019 04:05:02 GMT -->
</html>