@extends('front.master')

@section('title')
	Cart show
@endsection('title')

@section('body')
<hr/><br/>
<div class="box" align="center"><h1><i>My shopping cart</i></h1></div><br/><br/>

	<style type="text/css">
		.box2{
			width: 1000px;
			margin-left: 200px;
		}
		#result{
			width: 500px;
			margin-left: 500px;
		}
		#btn{
			margin-left: 700px;
		}
		#btn2{
			margin-left: 245px;
		}

	</style>
<div class='box2'>
<h1 style="color:red;">	{{ Session::get('sms') }}  </h1>
<table align='' class='table table-hover table-striped table-bordered' >
			<tr align="center">
				<th>SI No</th>              
				<th> Name</th>
				<th> Image</th>
				<th> Price TK.</th>
				<th> Quantity</th>
				<th>Total price TK</th>
				<th>Action</th>
			</tr>
			@php($i=1)
			@php($sum=0)
			@foreach($cartProduct as $product)
			<tr>
				<td align="center">{{ $i++ }}</td>
				<td align="center">{{ $product->name }}</td>
				<td align="center"><img src="{{ asset( $product->attributes->image ) }}" height="80" width="80"></td>
				<td align="center">{{ $product->price }}</td>
				<td align="center">

				<form action="{{ route('cartEdit') }}" method="POST">	
					@csrf
				{{ $product->quantity }}	
				<input type="hidden" name='id' value="{{ $product->id }}" >
				<input type='hidden' value='Edit' name='btn'>
				</form>
				
				</td>
				<td align="center">{{ $total = $product->price*$product->quantity }}</td>
				<td align="center">
					<a href="{{ route('deleteCart',['id'=>$product->id]) }}" onclick="return confirm('Are you sure?')">Delete</a>
				</td>
			</tr>
			<?php $sum = $sum+$total; ?>
			@endforeach	
		</table>
		
		<table class="table table-bordered table-striped table-dark table-hover" id='result'>
			<tr align="center">	
				<th align="center">Total item price (TK.) </th>	
				<td align="center">{{ $sum }}</td>
			</tr>
			<tr align="center">	
				<th align="center">Vat total (TK.) </th>	
				<td align="center">{{ $vat = 0 }}</td>
			</tr>
			<tr align="center">	
				<th align="center">Grand total (TK.) </th>	
				<td align="center">{{ $orderTotal = $sum+$vat }}</td>
				<?php
					Session::put('orderTotal',$orderTotal);
				?>
			</tr>							
		</table>
	</div>
	<br/><br/>
	<a href="{{ route('/') }}" id='btn' class='btn btn-success'>Continue shopping</a>
	@if(Session::get('customerId') && Session::get('shippingId'))
		<a href="{{ route('payment') }}" id='btn2' class='btn btn-success'>Checkout</a> <br/><br/>
	@elseif(Session::get('customerId'))
	    <a href="{{ route('shipping') }}" id='btn2' class='btn btn-success'>Checkout</a> <br/><br/>	
	@else   
		@if(!$count == 0)
		<a href="{{ route('checkout') }}" id='btn2' class='btn btn-success'>Checkout</a> <br/><br/> 
		@else

		@endif
	@endif    
@endsection('body')