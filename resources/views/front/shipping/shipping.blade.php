@extends('front.master')

@section('title')
	Shipping
@endsection('title')

@section('body')

<style type="text/css">
	#box{
		
	}
	
</style>
	
  <hr/>

  <h1 align="center">Shipping info</h1>

<p align="center">Dear {{ Session::get('customerName') }}. You have to give us product shipping info to complete your valuale order. If your billing info & shipping info are same then just press on save shipping info button.</p>

 <div id='box'>
<table width="550" align="center">
	<form action="{{ route('shippingSave') }}" method="POST">
	@csrf
	<tr>
		<td></td>
		<td height="60"><input type="text" value="{{ $customerId->name }}" class='form-control' name="name"></td>
	</tr>
	<tr>
		<td></td>
		<td height="60"><input type="email" value="{{ $customerId->email }}" class='form-control' name="email"></td>
	</tr>	
	<tr>
		<td></td>
		<td height="60"><input type="text" value="{{ $customerId->number }}" class='form-control' name="number"></td>
	</tr>	
	<tr>
		<td></td>
		<td height="60"><textarea name="address" class='form-control' >{{ $customerId->address }}</textarea></td>
	</tr>	
	<tr>
		<td></td>
		<td height="60"><input type="submit" class='btn btn-success' value="Save shipping info" name="btn"></td>
	</tr>	

 	</form>
	
	<tr>
	<td></td>
		<td height="60"><a href="{{ route('/') }}"class='btn btn-success' >Not buy at now</a></td>
	</tr>

</table>

</div>              

@endsection('body')