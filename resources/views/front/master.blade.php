<!doctype html>
<html lang="zxx">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title')</title>
    <link rel="icon" href="{{ asset('/') }}/front/img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('/') }}/front/css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="{{ asset('/') }}/front/css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="{{ asset('/') }}/front/css/owl.carousel.min.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="{{ asset('/') }}/front/css/all.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="{{ asset('/') }}/front/css/flaticon.css">
    <link rel="stylesheet" href="{{ asset('/') }}/front/css/themify-icons.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="{{ asset('/') }}/front/css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="{{ asset('/') }}/front/css/slick.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="{{ asset('/') }}/front/css/style.css">
</head>

<body>
    <!--::header part start::-->
    <header class="main_menu home_menu">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-11">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="{{ asset('/') }}"> <img src="{{ asset('/') }}/front/img/logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="menu_icon"><i class="fas fa-bars"></i></span>
                        </button>


                        <div class="collapse navbar-collapse main-menu-item" id="navbarSupportedContent">
                           


                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('/') }}">Home</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="blog.html" id="navbarDropdown_1"
                                        role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Shop
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown_1">
                                        <a class="dropdown-item" href="{{ route('product_category') }}"> shop category</a>
                                      
                                   <!--     <a class="dropdown-item" href="">product details</a>  -->
                                        
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="blog.html" id="navbarDropdown_3"
                                        role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Customer
                                 <style type="text/css">
                                     #out{
                                        cursor: pointer;
                                     }
                                 </style>
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown_2">
                                      @if(Session::get('customerId'))
                                        <a class="dropdown-item" onclick="document.getElementById('customerLoguotForm').submit();"> 
                                          <div id='out'>  Logout </div>
                                       </a>
                                       <form id='customerLoguotForm' action="{{ route('customLogout') }}" method="POST">
                                         @csrf  
                                       </form>
                                      @else 
                                        <a class="dropdown-item" href="{{ route('checkout') }}"> 
                                            Login
                                       </a>
                                      @endif 
                                        <a class="dropdown-item" href="{{ route('cartShow') }}">shopping cart</a>
                                       
                                        @if(Session::get('customerId'))
                                        <a class="dropdown-item" href="#">Shipping</a>
                                        @else 

                                        @endif  
                                        
                                    </div>
                                </li>
                                
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="blog.html" id="navbarDropdown_2"
                                        role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        blog
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown_2">
                                        <a class="dropdown-item" href="{{ route('blog') }}"> blog</a>
                                    </div>
                                </li>
                                
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('contact') }}">Contact</a>
                                </li>
                            </ul>
                        </div>
                        <div class="hearer_icon d-flex">
                            {{ Session::get('customerName') }}
                            <div class="">
                       
                                 
                                <a href="{{ route('cartShow') }}"><i class="ti-bag">Cart(
                                    @if($count == 0 )
                                        Empty
                                    @else
                                       {{ $count }}
                                     @endif )</i></a>
                                <!-- <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <div class="single_product">
    
                                    </div>
                                </div> -->
                            </div>
                          
                            <a id="search_1" href="javascript:void(0)"><i class="ti-search"></i></a>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
        <div class="search_input" id="search_input_box">
            <div class="container ">
                <form class="d-flex justify-content-between search-inner">
                    <input type="text" class="form-control" id="search_input" placeholder="Search Here">
                    <button type="submit" class="btn"></button>
                    <span class="ti-close" id="close_search" title="Close Search"></span>
                </form>
            </div>
        </div>
    </header>
    <!-- Header part end-->

    <!-- banner part start-->
    


@yield('body')

    <!--::subscribe_area part end::-->

    <!--::footer_part start::-->
    <footer class="footer_part">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-sm-6 col-lg-2">
                    <div class="single_footer_part">
                        <h4>Category</h4>
                        <ul class="list-unstyled">
                            <li><a href="#">Male</a></li>
                            <li><a href="#">Female</a></li>
                            <li><a href="#">Shoes</a></li>
                            <li><a href="#">Fashion</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-2">
                    <div class="single_footer_part">
                        <h4>Company</h4>
                        <ul class="list-unstyled">
                            <li><a href="">About</a></li>
                            <li><a href="">News</a></li>
                            <li><a href="">FAQ</a></li>
                            <li><a href="">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="single_footer_part">
                        <h4>Address</h4>
                        <ul class="list-unstyled">
                            <li><a href="#">200, Green block, NewYork</a></li>
                            <li><a href="#">+10 456 267 1678</a></li>
                            <li><span>contact89@winter.com</span></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="single_footer_part">
                        <h4>Newsletter</h4>
                        <div id="mc_embed_signup">
                            <form target="_blank"
                                action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscribe_form relative mail_part">
                                <input type="email" name="email" id="newsletter-form-email" placeholder="Email Address"
                                    class="placeholder hide-on-focus" onfocus="this.placeholder = ''"
                                    onblur="this.placeholder = ' Email Address '">
                                <button type="submit" name="submit" id="newsletter-submit"
                                    class="email_icon newsletter-submit button-contactForm">subscribe</button>
                                <div class="mt-10 info"></div>
                            </form>
                        </div>
                        <div class="social_icon">
                            <a href="#"><i class="ti-facebook"></i></a>
                            <a href="#"><i class="ti-twitter-alt"></i></a>
                            <a href="#"><i class="ti-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="copyright_text">
                        <P><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></P>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--::footer_part end::-->

    <!-- jquery plugins here-->
    <script src="{{ asset('/') }}/front/js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="{{ asset('/') }}/front/js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="{{ asset('/') }}/front/js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="{{ asset('/') }}/front/js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="{{ asset('/') }}/front/js/swiper.min.js"></script>
    <!-- swiper js -->
    <script src="{{ asset('/') }}/front/js/mixitup.min.js"></script>
    <!-- particles js -->
    <script src="{{ asset('/') }}/front/js/owl.carousel.min.js"></script>
    <script src="{{ asset('/') }}/front/js/jquery.nice-select.min.js"></script>
    <!-- slick js -->
    <script src="{{ asset('/') }}/front/js/slick.min.js"></script>
    <script src="{{ asset('/') }}/front/js/jquery.counterup.min.js"></script>
    <script src="{{ asset('/') }}/front/js/waypoints.min.js"></script>
    <script src="{{ asset('/') }}/front/js/contact.js"></script>
    <script src="{{ asset('/') }}/front/js/jquery.ajaxchimp.min.js"></script>
    <script src="{{ asset('/') }}/front/js/jquery.form.js"></script>
    <script src="{{ asset('/') }}/front/js/jquery.validate.min.js"></script>
    <script src="{{ asset('/') }}/front/js/mail-script.js"></script>
    <!-- custom js -->
    <script src="{{ asset('/') }}/front/js/custom.js"></script>
</body>

</html>