@extends('front.master')

@section('title')
	Checkout
@endsection('title')

@section('body')


  <!-- breadcrumb start-->

  <!--================Checkout Area =================-->
  <section class="checkout_area section_padding">
    <div class="container">
      <div class="returning_customer">
 
 <b><i><h1> &nbsp;&nbsp;&nbsp;Customers Log in </h1></i></b>
        <p>
          If you have shopped with us before, please enter your details in the
          boxes below. If you are a new customer, please proceed to the
          Billing & Shipping section.
        </p>
        <form class="row contact_form" action="{{ route('customers_login') }}" method="POST" novalidate="novalidate">
          @csrf
          <div class="col-md-6 form-group p_star">
            <input type="text" class="form-control" placeholder="Username or email*" name="email"  />
           
          </div>
          <div class="col-md-6 form-group p_star">
            <input type="password" class="form-control" placeholder="Password*" id="password" name="password"  /><mark> {{ Session::get('message') }} </mark>
           
          </div>
          <div class="col-md-12 form-group">
            <button type="submit" value="Log in" name='btn' class="btn_3">
              log in
            </button>
            <div class="creat_account">
              <input type="checkbox" disabled="" id="f-option" name="selector" />
              <label for="f-option">Remember me</label>
            </div>
            <div class="lost_pass" onlyread href="#">Lost your password?</div>
          </div>
        </form>
      </div>
      <hr/>



      <div class="billing_details">
        <div class="row">
          <div class="col-lg-8">
           <b><i><h1> &nbsp;&nbsp;Customers Resigtration </h1></i></b>
            <form class="row contact_form" action="{{ route('costomer_registration') }}" method="POST" novalidate="novalidate">
              @csrf
              <div class="col-md-12 form-group p_star">
                <input type="text" class="form-control" id="first" placeholder="Name*" name="name" />
               {{ $errors->has('name') ? $errors->first('name'):'' }}
              </div>
              
              <div class="col-md-6 form-group p_star">
                <input type="text" class="form-control" id="number" placeholder="Phone number*" name="number" />
               {{ $errors->has('number') ? $errors->first('number'):'' }}
              </div>

               <div class="col-md-6 form-group p_star">
                <input type="text" class="form-control" id="email" placeholder="Email Address*" name="email" /> {{ $errors->has('email') ? $errors->first('email'):'' }}
            &nbsp; <span id='result'></span> 
              </div>

              <div class="col-md-6 form-group p_star">
                <input type="password" class="form-control" placeholder="Password*" name="password" />
                {{ $errors->has('password') ? $errors->first('password'):'' }}
              </div>
                
              <div class="col-md-12 ">
             <textarea name='address' placeholder="Address*" rows="4" cols="70"></textarea>  
              </div>
       &nbsp; &nbsp;    {{ $errors->has('address') ? $errors->first('address'):'' }}
              <div class="col-md-12 form-group">
                 <div class="col-md-12 form-group"><br/>
           	  <button type="submit" value="submit" id='regBtn' class="btn_3">
             Create account
            </button>
           </form> 
              </div>
            <hr/>  
              
  <!--================End Checkout Area =================-->

  <!-- subscribe_area part start-->
  <section class="instagram_photo">
      <div class="container-fluid>
          <div class="row">
              <div class="col-lg-12">
                  <div class="instagram_photo_iner">
                      <div class="single_instgram_photo">
                          <img src="{{ asset('/') }}/front/img/instagram/inst_1.png" alt="">
                          <a href="#"><i class="ti-instagram"></i></a> 
                      </div>
                      <div class="single_instgram_photo">
                          <img src="{{ asset('/') }}/front/img/instagram/inst_2.png" alt="">
                          <a href="#"><i class="ti-instagram"></i></a> 
                      </div>
                      <div class="single_instgram_photo">
                          <img src="{{ asset('/') }}/front//img/instagram/inst_3.png" alt="">
                          <a href="#"><i class="ti-instagram"></i></a> 
                      </div>
                      <div class="single_instgram_photo">
                          <img src="{{ asset('/') }}/front//img/instagram/inst_4.png" alt="">
                          <a href="#"><i class="ti-instagram"></i></a> 
                      </div>
                      <div class="single_instgram_photo">
                          <img src="{{ asset('/') }}/front//img/instagram/inst_5.png" alt="">
                          <a href="#"><i class="ti-instagram"></i></a> 
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>

<script>

 var input = document.getElementById('email');
 input.oninput = function() {

    var xmlHttp = new XMLHttpRequest();
    var mail = document.getElementById('email').value;
    var serverPage = 'http://localhost/gear-buffets/public/ajax/'+mail;
    xmlHttp.open('GET',serverPage);
    xmlHttp.onreadystatechange = function() {
          
      if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
        document.getElementById('result').innerHTML = xmlHttp.responseText;
            if(xmlHttp.responseText == 'Already Exists'){
                document.getElementById('regBtn').disabled = true;
                document.getElementById('regBtn').style.marginLeft = "-350px";
            }else{
                document.getElementById('regBtn').disabled = false;
                document.getElementById('regBtn').style.marginLeft = "0px";
            }
      }
    }
    xmlHttp.send(null);
  }
</script>

@endsection('body')

