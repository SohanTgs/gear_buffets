@extends('front.master')

@section('title')
	Payments
@endsection('title')

@section('body')
	
	<h4 align="center">Dear <i>{{ Session::get('customerName') }}</i>. You have to give us product payment method</h4>
<style type="text/css">
	#table{
		width: 700px;
	}
	#table2{
		margin-left: 320px;
	}
</style>

<hr/>
<br/>
	<form action="{{ route('order') }}" method="POST">
		@csrf
		<div id='table'>
		<table class='table table-dark table-hover table-striped' id='table2'>
			<tr>
				<th><label for='cash'>Cash on delivery</label></th>
				<td><input type="radio" name="payment_type" id='cash' checked="" value='Cash'> Cash on delivery</td>
			</tr>
			<tr>
				<th><label for='paypal'>Paypal</label></th>
				<td><input type="radio" name="payment_type" id='paypal' value='Paypal'> Paypal</td>
			</tr>
			<tr>
				<th><label for='bKash'>bKash</label></th>
				<td><input type="radio" name="payment_type" id='bKash' value='bKash'> bKash</td>
			</tr>	
			<tr>
				<th></th>
				@if(!$count == 0)
				<td><input type="submit" name="btn" value='Confirm order' class='btn btn-success'></td>
				@else
					<h2>Your shopping cart is empty....</h2>
				@endif	
			</tr>					
		</table>
		</div>
	</form>
<br/>
@endsection('body')