@extends('front.master')

@section('title')
	category
@endsection

@section('body')

<section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner">
                        <div class="breadcrumb_iner_item">
                            <p>Home / Category</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!-- breadcrumb start-->

    <!--================Category Product Area =================-->
    <section class="cat_product_area section_padding border_top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="left_sidebar_area">
                        <aside class="left_widgets p_filter_widgets sidebar_box_shadow">
                            <div class="l_w_title">
                                <h3>Browse Categories</h3>
                            </div>
                            <div class="widgets_inner">
                                <ul class="list">
                                  @foreach($categories as $category)
                                    <li>
                                        <a href="{{ route('product_category',['id'=>$category->id]) }}">{{ $category->category_name }}</a>
                                    </li>
                                  @endforeach


                           <!--   <li class="sub-menu">
                                        <a href="#Electronics" class=" d-flex justify-content-between">
                                            Electronics
                                            <div class="right ti-plus"></div>
                                        </a>
                                        <ul>
                                            <li>
                                                <a href="#Electronics">Home Appliances</a>
                                            </li>
                                            <li>
                                                <a href="#Electronics">Smartphones</a>
                                            </li>
                                            <li>
                                                <a href="#Electronics">Kitchen Appliances</a>
                                            </li>
                                            <li>
                                                <a href="#Electronics">Computer Accessories</a>
                                            </li>
                                            <li>
                                                <a href="#Electronics">Meat Alternatives</a>
                                            </li>
                                            <li>
                                                <a href="#Electronics">Appliances</a>
                                            </li>
                                        </ul>
                                    </li>  -->

                                </ul>
                            </div>
                        </aside>
                   </div>
                </div>
                
     

     
@if(isset($_GET['id']))
   
    @foreach($product as $result)

    <div class="col-lg-4 col-sm-6">
                            <div class="single_category_product">
                                <div class="single_category_img">
                                    <img src="{{ asset($result->product_image) }}" height="400" alt="">
                                    <div class="category_social_icon">
                                        <ul>
                                            <li><a href="#"><i class="ti-heart"></i></a></li>
                                            <li><a href="#"><i class="ti-bag"></i></a></li>
                                        </ul>
                                       
                                    </div>
                                    <div class="category_product_text">
                                        <a href="single-product.html"><h5>{{ $result->product_name }}</h5></a>
                                        <p>{{ $result->product_price }}</p>
                                        <a href="{{ route('details',['id'=>$result->id]) }}" class='btn btn-success'>Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
    @endforeach

@else
        @foreach($allProducts as $products)
            <div class="col-lg-4 col-sm-6">
                            <div class="single_category_product">
                                <div class="single_category_img">
                                    <img src="{{ asset($products->product_image) }}" height="400" alt="">
                                    <div class="category_social_icon">
                                        <ul>
                                            <li><a href="#"><i class="ti-heart"></i></a></li>
                                            <li><a href="#"><i class="ti-bag"></i></a></li>
                                        </ul>

                                    </div>
                                    <div class="category_product_text">
                                        <a href="single-product.html"><h5>{{ $products->product_name }}</h5></a>
                                        <p>{{ $products->product_price }}</p>
                                        <a href="{{ route('details',['id'=>$products->id]) }}" class='btn btn-success'>Details</a>
                                    </div>
                                </div>
                            </div>
                     </div>
        @endforeach    
        {{ $allProducts->links() }}
@endif
                       





                        <div class="col-lg-12 text-center">
                            <a href="#" class="btn_2">More Items</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Category Product Area =================-->

    <!-- free shipping here -->
    <section class="shipping_details section_padding border_top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="single_shopping_details">
                        <img src="{{ asset('/') }}/front/img/icon/icon_1.png" alt="">
                        <h4>Free shipping</h4>
                        <p>Divided face for bearing the divide unto seed winged divided light Forth.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_shopping_details">
                        <img src="{{ asset('/') }}/front/img/icon/icon_2.png" alt="">
                        <h4>Free shipping</h4>
                        <p>Divided face for bearing the divide unto seed winged divided light Forth.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_shopping_details">
                        <img src="{{ asset('/') }}/front/img/icon/icon_3.png" alt="">
                        <h4>Free shipping</h4>
                        <p>Divided face for bearing the divide unto seed winged divided light Forth.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_shopping_details">
                        <img src="{{ asset('/') }}/front/img/icon/icon_4.png" alt="">
                        <h4>Free shipping</h4>
                        <p>Divided face for bearing the divide unto seed winged divided light Forth.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- free shipping end -->
    
    <!-- subscribe_area part start-->
    <section class="instagram_photo">
        <div class="container-fluid>
            <div class="row">
                <div class="col-lg-12">
                    <div class="instagram_photo_iner">
                        <div class="single_instgram_photo">
                            <img src="{{ asset('/') }}/front/img/instagram/inst_1.png" alt="">
                            <a href="#"><i class="ti-instagram"></i></a> 
                        </div>
                        <div class="single_instgram_photo">
                            <img src="{{ asset('/') }}/front/img/instagram/inst_2.png" alt="">
                            <a href="#"><i class="ti-instagram"></i></a> 
                        </div>
                        <div class="single_instgram_photo">
                            <img src="{{ asset('/') }}/front/img/instagram/inst_3.png" alt="">
                            <a href="#"><i class="ti-instagram"></i></a> 
                        </div>
                        <div class="single_instgram_photo">
                            <img src="{{ asset('/') }}/front/img/instagram/inst_4.png" alt="">
                            <a href="#"><i class="ti-instagram"></i></a> 
                        </div>
                        <div class="single_instgram_photo">
                            <img src="{{ asset('/') }}/front/img/instagram/inst_5.png" alt="">
                            <a href="#"><i class="ti-instagram"></i></a> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
