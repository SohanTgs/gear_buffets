-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 21, 2020 at 05:39 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brand_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `brand_name`, `brand_description`, `publication_status`, `created_at`, `updated_at`) VALUES
(17, 'Bata', 'all products', 1, '2020-03-01 07:44:21', '2020-03-01 07:44:21'),
(16, 'Hp', 'all hp assets', 1, '2020-03-01 07:32:12', '2020-03-01 07:32:28'),
(15, 'Tiens', 'all tiens producs', 1, '2020-03-01 07:31:41', '2020-03-01 07:31:41'),
(14, 'iPhone', 'all iphone produc', 0, '2020-03-01 07:31:13', '2020-03-01 07:32:20'),
(12, 'Tecno', 'all tecno produts', 1, '2020-03-01 07:30:33', '2020-03-02 01:35:32'),
(13, 'Walton', 'all walton products', 1, '2020-03-01 07:30:54', '2020-03-01 07:30:54'),
(18, 'flat', 'test', 1, '2020-03-01 10:12:48', '2020-03-02 01:25:35'),
(19, 'sdf', 'sdf', 1, '2020-03-02 01:29:11', '2020-03-02 01:35:24'),
(20, 'sdsd', 'sdsd', 1, '2020-03-02 01:42:59', '2020-03-02 01:49:22'),
(21, '10', '10', 0, '2020-03-02 02:13:59', '2020-03-02 02:13:59'),
(22, '111', '11', 1, '2020-03-02 02:14:14', '2020-03-02 02:14:14');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `category_description`, `publication_status`, `created_at`, `updated_at`) VALUES
(31, 'Harbal', 'Like paste', 1, '2020-03-01 07:29:25', '2020-03-02 05:40:54'),
(28, 'Sports', 'Here you get all kind of sports assets', 1, '2020-03-01 07:28:07', '2020-03-01 07:28:07'),
(29, 'Cloths', 'All kind of cloths', 1, '2020-03-01 07:28:28', '2020-03-01 07:28:28'),
(30, 'Electronics', 'All necessary electronics products', 1, '2020-03-01 07:29:06', '2020-03-01 07:29:06'),
(27, 'Woman', 'This category is only for woman', 1, '2020-03-01 07:27:33', '2020-03-01 07:27:33'),
(26, 'Men', 'This category for only men', 1, '2020-03-01 07:27:03', '2020-03-02 01:23:47'),
(32, 'Flat', 'Flat or Plot', 0, '2020-03-01 07:29:46', '2020-03-02 01:21:56'),
(34, 'sdf', 'sdf', 0, '2020-03-02 01:28:23', '2020-03-02 05:40:43'),
(35, 'fsdfggggg', 'sdfsdf', 0, '2020-03-02 01:40:15', '2020-03-02 01:50:19'),
(36, 'asas', 'aaaaaaaaaa', 0, '2020-03-02 02:11:30', '2020-03-02 02:11:30');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `number`, `email`, `password`, `address`, `created_at`, `updated_at`) VALUES
(7, 'Md Rajib Hossain', '01840999640', 'rajib@gmail.com', '$2y$10$6FyT6KOAuR8d5mDkVj3Hcua./rAEXnSBFlqIb9e4tJoYJ0XvOLNr2', 'Mirpur dhaka 1216', '2020-03-07 09:00:17', '2020-03-07 09:00:17');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_02_26_170002_create_categories_table', 2),
(4, '2020_02_26_170715_create_categories_table', 3),
(5, '2020_02_28_172240_create_brands_table', 4),
(6, '2020_02_29_150254_create_products_table', 5),
(7, '2020_03_05_123828_create_custoemrs_table', 6),
(8, '2020_03_05_145417_create_shippings_table', 7),
(9, '2020_03_05_161936_create_orders_table', 8),
(10, '2020_03_05_162019_create_payments_table', 8),
(11, '2020_03_05_162105_create_order_details_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `shipping_id` int(11) NOT NULL,
  `order_total` double(10,2) NOT NULL,
  `order_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `shipping_id`, `order_total`, `order_status`, `created_at`, `updated_at`) VALUES
(24, 7, 18, 10.00, 'Pending', '2020-03-14 12:39:30', '2020-03-14 12:39:30'),
(23, 7, 17, 20000.00, 'Pending', '2020-03-14 03:39:06', '2020-03-14 03:39:06'),
(20, 7, 14, 20000.00, 'Pending', '2020-03-10 07:17:51', '2020-03-10 07:17:51'),
(15, 7, 10, 10000.00, 'Completed', '2020-03-09 07:08:26', '2020-03-09 07:16:21'),
(16, 7, 11, 45000.00, 'Completed', '2020-03-09 07:25:14', '2020-03-09 07:30:25'),
(21, 7, 15, 65000.00, 'Pending', '2020-03-10 08:05:22', '2020-03-10 08:05:22'),
(22, 7, 16, 10000.00, 'Pending', '2020-03-13 12:26:56', '2020-03-13 12:26:56'),
(25, 7, 18, 0.00, 'Pending', '2020-03-14 12:40:19', '2020-03-14 12:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
CREATE TABLE IF NOT EXISTS `order_details` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` double(10,2) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `product_id`, `product_name`, `product_price`, `product_quantity`, `created_at`, `updated_at`) VALUES
(23, 21, 13, 'Tecno kemon isky3', 10000.00, 3, '2020-03-10 08:05:22', '2020-03-10 08:05:22'),
(15, 16, 13, 'Tecno kemon isky3', 10000.00, 1, '2020-03-09 07:25:14', '2020-03-09 07:25:14'),
(17, 18, 23, 'ssdfsdf', 5.00, 1, '2020-03-09 08:02:48', '2020-03-09 08:02:48'),
(22, 20, 13, 'Tecno kemon isky3', 10000.00, 2, '2020-03-10 07:17:51', '2020-03-10 07:17:51'),
(12, 14, 15, 'Dress', 1000.00, 2, '2020-03-07 09:31:30', '2020-03-07 09:31:30'),
(13, 15, 13, 'Tecno kemon isky3', 10000.00, 1, '2020-03-09 07:08:26', '2020-03-09 07:08:26'),
(25, 22, 13, 'Tecno kemon isky3', 10000.00, 1, '2020-03-13 12:26:56', '2020-03-13 12:26:56'),
(20, 19, 15, 'Dress', 1000.00, 1, '2020-03-09 08:17:39', '2020-03-09 08:17:39'),
(21, 19, 16, 'Bata shoes', 3000.00, 1, '2020-03-09 08:17:39', '2020-03-09 08:17:39'),
(24, 21, 14, 'Laptop', 35000.00, 1, '2020-03-10 08:05:22', '2020-03-10 08:05:22'),
(26, 23, 13, 'Tecno kemon isky3', 10000.00, 2, '2020-03-14 03:39:06', '2020-03-14 03:39:06'),
(27, 24, 22, 'ss', 10.00, 1, '2020-03-14 12:39:30', '2020-03-14 12:39:30');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('tgs.sohan@gmail.com', '$2y$10$BDkh/YTi4Gwqj5tNnoLmleMXNo3tpfhfvwkh68r5Xu1XQ2pKYrVgW', '2020-02-26 05:42:18');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
CREATE TABLE IF NOT EXISTS `payments` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `payment_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `order_id`, `payment_type`, `payment_status`, `created_at`, `updated_at`) VALUES
(23, 23, 'Cash', 'Pending', '2020-03-14 03:39:06', '2020-03-14 03:39:06'),
(22, 22, 'Cash', 'Pending', '2020-03-13 12:26:56', '2020-03-13 12:26:56'),
(21, 21, 'Cash', 'Pending', '2020-03-10 08:05:22', '2020-03-10 08:05:22'),
(16, 16, 'Cash', 'Completed', '2020-03-09 07:25:14', '2020-03-09 07:30:25'),
(15, 15, 'Cash', 'Completed', '2020-03-09 07:08:26', '2020-03-09 07:16:21'),
(20, 20, 'Cash', 'Pending', '2020-03-10 07:17:51', '2020-03-10 07:17:51'),
(24, 24, 'Cash', 'Pending', '2020-03-14 12:39:30', '2020-03-14 12:39:30'),
(25, 25, 'Cash', 'Pending', '2020-03-14 12:40:19', '2020-03-14 12:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` double(10,2) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `long_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `brand_id`, `product_name`, `product_price`, `product_quantity`, `short_description`, `long_description`, `product_image`, `publication_status`, `created_at`, `updated_at`) VALUES
(14, 30, 16, 'Laptop', 35000.00, 1, 'HP 15-db0083AX AMD Dual Core 15.6 Inch HD Laptop with Genuine Windows 10', 'AMD Dual Core A4-9125 (1MB Cache , 2.30GHz up to 2.60GHz) Processor 4GB DDR4 RAM 500GB HDD 15.6\" LED HD ( 1366x768 )Display', 'product-image/lapto.jpg', 1, '2020-03-01 07:41:06', '2020-03-01 07:43:28'),
(13, 30, 12, 'Tecno kemon isky3', 10000.00, 1, 'Tecno Camon iSky 3 smartphone was launched in March 2019.', 'The phone comes with a 6.20-inch touchscreen display and an aspect ratio of 19:9.\r\nTecno Camon iSky 3 is powered by a 2GHz quad-core processor. It comes with 2GB of RAM.', 'product-image/tecno.jpg', 1, '2020-03-01 07:39:35', '2020-03-01 07:39:35'),
(15, 27, 17, 'Dress', 1000.00, 1, 'Bangladeshi shirt', 'Casual Shirts for Men - Buy Men Casual Shirt Online in India', 'product-image/women.jpg', 1, '2020-03-01 07:46:17', '2020-03-01 07:46:17'),
(16, 26, 17, 'Bata shoes', 3000.00, 1, 'North Star Daniel Lace-Up Free Time Shoe for Men', 'With the upper made from the soft corduroy, this free time shoe by North Star is trendy one for everyday use. Designed for spring & autumn days.', 'product-image/bata.jpg', 1, '2020-03-01 07:47:31', '2020-03-01 07:52:15'),
(17, 28, 15, 'Sohan', 10000.00, 1, 'My name is sohan', 'Im banglasdehis manush ami vat khai tomara vat khete chao ki ?', 'product-image/IMG_20160118_124755.jpg', 1, '2020-03-01 07:53:43', '2020-03-02 08:47:29'),
(18, 28, 16, 'dfsa', 10.00, 1, 'fg', 'fg', 'product-image/20161212_074152.jpg', 0, '2020-03-02 01:29:46', '2020-03-02 07:11:27'),
(19, 28, 15, 'dsf', 1.00, 1, 'd0', 'd', 'product-image/2017-04-19-13-13-13-663.jpg', 1, '2020-03-02 01:31:42', '2020-03-02 08:47:26'),
(21, 30, 12, 'Okay', 5.00, 1, 'sds', 'fsdf', 'product-image/2018-02-28-20-28-20-905.jpg', 1, '2020-03-02 01:55:30', '2020-03-02 08:47:16'),
(22, 28, 16, 'ss', 10.00, 1, 'fgf', 'ff', 'product-image/IMG_20190708_133120_8.jpg', 1, '2020-03-02 03:23:54', '2020-03-13 10:18:33'),
(23, 33, 18, 'ssdfsdf', 5.00, 1, 'dsf', 'sdf', 'product-image/2017-01-21-10-53-41-730.jpg', 1, '2020-03-02 03:24:24', '2020-03-02 08:46:47');

-- --------------------------------------------------------

--
-- Table structure for table `shippings`
--

DROP TABLE IF EXISTS `shippings`;
CREATE TABLE IF NOT EXISTS `shippings` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shippings`
--

INSERT INTO `shippings` (`id`, `name`, `email`, `number`, `address`, `created_at`, `updated_at`) VALUES
(13, 'Md Rajib Hossain', 'rajib@gmail.com', '01840999640', 'Mirpur dhaka 1216', '2020-03-09 08:02:45', '2020-03-09 08:02:45'),
(12, 'Md Rajib Hossain', 'rajib@gmail.com', '01840999640', 'Mirpur dhaka 1216', '2020-03-09 07:30:59', '2020-03-09 07:30:59'),
(11, 'Md Rajib Hossain', 'rajib@gmail.com', '01840999640', 'Mirpur dhaka 1216', '2020-03-09 07:25:12', '2020-03-09 07:25:12'),
(9, 'Md Rajib Hossain', 'rajib@gmail.com', '01840999640', 'Mirpur dhaka 1216', '2020-03-07 09:31:28', '2020-03-07 09:31:28'),
(10, 'Md Rajib Hossain', 'rajib@gmail.com', '01840999640', 'Mirpur dhaka 1216', '2020-03-09 07:08:22', '2020-03-09 07:08:22'),
(14, 'Md Rajib Hossain', 'rajib@gmail.com', '01840999640', 'Mirpur dhaka 1216', '2020-03-10 07:17:45', '2020-03-10 07:17:45'),
(15, 'Md Rajib Hossain', 'rajib@gmail.com', '01840999640', 'Mirpur dhaka 1216', '2020-03-10 08:05:20', '2020-03-10 08:05:20'),
(16, 'Md Rajib Hossain', 'rajib@gmail.com', '01840999640', 'Mirpur dhaka 1216', '2020-03-13 12:26:55', '2020-03-13 12:26:55'),
(17, 'Md Rajib Hossain', 'rajib@gmail.com', '01840999640', 'Mirpur dhaka 1216', '2020-03-14 03:39:05', '2020-03-14 03:39:05'),
(18, 'Md Rajib Hossain', 'rajib@gmail.com', '01840999640', 'Mirpur dhaka 1216', '2020-03-14 12:39:29', '2020-03-14 12:39:29');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `mobile` int(11) DEFAULT NULL,
  `location` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `mobile`, `location`, `bio`, `image`) VALUES
(10, 'Mohammad Sohan', 'tgs.sohan@gmail.com', NULL, '$2y$10$MR3Bro/ix00KQCxcQvD2XukiwqlxLxCvMhA1lZBtBdRNW95exijVu', NULL, '2020-03-10 02:35:04', '2020-03-13 09:49:52', 1918134892, 'Bangladesh', 'I\'m a simple web developer using by PHP,MySQL and laravel \r\nAnd this is my second laravel project', 'admin-image/IMG_20190708_110838_9.jpg'),
(11, 'Mohammad Rajib', 'rajib@gmail.com', NULL, '$2y$10$Wa6FrdGJNRS9r0PiPX6yWOZ8fbMeafYLMtwE9.g1FcWUhjYdyATVK', NULL, '2020-03-10 03:50:08', '2020-03-10 03:51:42', 1840999640, 'Bangladesh', 'Senior web developer using PHP and expert in Laravel & MySQL', 'admin-image/IMG_20180504_023645.jpg');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
