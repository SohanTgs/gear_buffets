<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[
	'uses'=>'Practice@index',
	'as'  =>'/'
]);

Route::get('/product_category',[
	'uses'=>'Practice@product_category',
	'as'  =>'product_category'
]);

Route::get('/details/{id}',[
	'uses'=>'Practice@details',
	'as'  =>'details'
]);


Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('home');


Route::group(['middleware' => ['Practice']], function () {

Route::get('dashboard/add_cetegory',[
	'uses'=>'Practice@add_cetegory',
	'as'  =>'add_cetegory'

]);
    
Route::post('/add_cetegory/category_save',[
	'uses'=>'Practice@category_save',
	'as'  =>'category_save'

]);

Route::get('dashboard/manage_category',[
	'uses'=>'Practice@manage_category',
	'as'  =>'manage_category'
]);

Route::get('dashboard/manage_category/Unpublished_category/{id}',[
	'uses'=>'Practice@Unpublished_category',
	'as'  =>'Unpublished_category'
]);

Route::get('dashboard/manage_category/published_category/{id}',[
	'uses'=>'Practice@published_category',
	'as'  =>'published_category'
]);

Route::get('dashboard/manage_category/update_category/{id}',[
	'uses'=>'Practice@update_category',
	'as'  =>'update_category'
]);

Route::POST('dashboard/manage_category/update_category/update_save',[
	'uses'=>'Practice@update_save',
	'as'  =>'update_save'
]);

Route::get('dashboard/manage_category/delete_category/{id}',[
	'uses'=>'Practice@delete_category',
	'as'  =>'delete_category'
]);

Route::get('dashboard/add_brand',[
	'uses'=>'Practice@add_brand',
	'as'  =>'add_brand'
]);

Route::get('dashboard/manage_brand',[
	'uses'=>'Practice@manage_brand',
	'as'  =>'manage_brand'
]);

Route::POST('dashboard/add_brand/save_brand',[
	'uses'=>'Practice@save_brand',
	'as'  =>'save_brand'
]);

Route::get('dashboard/manage_brand/brand_delete/{id}',[
	'uses'=>'Practice@brand_delete',
	'as'  =>'brand_delete'
]);

Route::get('dashboard/manage_brand/Unpublished_brand/{id}',[
	'uses'=>'Practice@Unpublished_brand',
	'as'  =>'Unpublished_brand'
]);

Route::get('dashboard/manage_brand/Published_brand/{id}',[
	'uses'=>'Practice@Published_brand',
	'as'  =>'Published_brand'
]);

Route::get('dashboard/manage_brand/update_brand/{id}',[
	'uses'=>'Practice@update_brand',
	'as'  =>'update_brand'
]);

Route::POST('dashboard/manage_brand/update_brand/brandUpdate_save',[
	'uses'=>'Practice@brandUpdate_save',
	'as'  =>'brandUpdate_save'
]);

Route::get('dashboard/add_product',[
	'uses'=>'Practice@add_product',
	'as'  =>'add_product'
]);

Route::get('dashboard/manage_product',[
	'uses'=>'Practice@manage_product',
	'as'  =>'manage_product'
]);

Route::POST('dashboard/add_product/new_product',[
	'uses'=>'Practice@new_product',
	'as'  =>'new_product'
]);

Route::get('dashboard/manage_product/product_delete/{id}',[
	'uses'=>'Practice@product_delete',
	'as'  =>'product_delete'
]);

Route::get('dashboard/manage_product/product_unpublished/{id}',[
	'uses'=>'Practice@product_unpublished',
	'as'  =>'product_unpublished'
]);

Route::get('dashboard/manage_product/product_published/{id}',[
	'uses'=>'Practice@product_published',
	'as'  =>'product_published'
]);

Route::get('dashboard/manage_product/product_update/{id}',[
	'uses'=>'Practice@product_update',
	'as'  =>'product_update'
]);

Route::POST('dashboard/manage_product/product_update/save_productUpdate',[
	'uses'=>'Practice@save_productUpdate',
	'as'  =>'save_productUpdate'
]);

Route::get('dashboard/profile',[
	'uses'=>'Practice@profile',
	'as'  =>'profile'
]);

Route::get('dashboard/settings',[
	'uses'=>'Practice@settings',
	'as'  =>'settings'
]);

Route::get('dashboard/manageOrder/compelteStatus/{id}',[
	'uses'=>'Practice@compelteStatus',
	'as'  =>'compelteStatus'
]);

Route::POST('dashboard/profile/profileUpdate',[
	'uses'=>'Practice@profileUpdate',
	'as'  =>'profileUpdate'
]);

Route::POST('dashboard/profile/profilePasswordChange',[
	'uses'=>'Practice@profilePasswordChange',
	'as'  =>'profilePasswordChange'
]);

Route::POST('dashboard/profile/adminPicture',[
	'uses'=>'Practice@adminPicture',
	'as'  =>'adminPicture'
]);

Route::get('/InboxMail',[
	'uses'=>'Sendmail@InboxMail',
	'as'  =>'InboxMail'
]);

Route::get('/InboxMail/MailDelete/{id}',[
	'uses'=>'Sendmail@MailDelete',
	'as'  =>'MailDelete'
]);

Route::POST('InboxMail/SendingMail',[
	'uses'=>'Sendmail@SendingMail',
	'as'  =>'SendingMail'
]);

Route::get('InboxMail/showSendEmail',[
	'uses'=>'Sendmail@showSendEmail',
	'as'  =>'showSendEmail'
]);

Route::get('InboxMail/showSendEmail/postMailDelete/{id}',[
	'uses'=>'Sendmail@postMailDelete',
	'as'  =>'postMailDelete'
]);

Route::get('/compose',[
	'uses'=>'Sendmail@compose',
	'as'  =>'compose'
]);



}); // middleware end syntax


Route::POST('/details/addCart',[
	'uses'=>'Practice@addCart',
	'as'  =>'addCart'
]);

Route::get('addCart/cartShow',[
	'uses'=>'Practice@cartShow',
	'as'  =>'cartShow'
]);

Route::get('addCart/cartShow/deleteCart/{id}',[
	'uses'=>'Practice@deleteCart',
	'as'  =>'deleteCart'
]);

Route::POST('addCart/cartShow/cartEdit',[
	'uses'=>'Practice@cartEdit',
	'as'  =>'cartEdit'
]);

Route::get('checkout',[
	'uses'=>'Practice@checkout',
	'as'  =>'checkout'
]);

Route::POST('checkout/costomer_registration',[
	'uses'=>'Practice@costomer_registration',
	'as'  =>'costomer_registration'
]);


Route::group(['middleware' => ['Front']], function () {

Route::get('checkout/shipping',[
	'uses'=>'Practice@shipping',
	'as'  =>'shipping',
]);

Route::POST('checkout/shipping/shippingSave',[
	'uses'=>'Practice@shippingSave',
	'as'  =>'shippingSave'
]);

Route::get('checkout/payment',[
	'uses'=>'Practice@payment',
	'as'  =>'payment',
	'middleware'=>'Shipping'
]);


}); // front middleware syntax

Route::POST('checkout/order',[
	'uses'=>'Practice@order',
	'as'  =>'order'
]);

Route::get('completeOrder',[
	'uses'=>'Practice@completeOrder',
	'as'  =>'completeOrder'
]);

Route::POST('/checkout/customers_login',[
	'uses'=>'Practice@customers_login',
	'as'  =>'customers_login'
]);

Route::POST('/checkout/customers_login',[
	'uses'=>'Practice@customers_login',
	'as'  =>'customers_login'
]);

Route::POST('/checkout/customLogout',[
	'uses'=>'Practice@customLogout',
	'as'  =>'customLogout'
]);

Route::get('dashboard/manageOrder',[
	'uses'=>'Practice@manageOrder',
	'as'  =>'manageOrder',
	'middleware'=>'Practice'
]);

Route::get('dashboard/manageOrder/viewOrder/{id}',[
	'uses'=>'Practice@viewOrder',
	'as'  =>'viewOrder'
]);

Route::get('dashboard/manageOrder/invoice/{id}',[
	'uses'=>'Practice@invoice',
	'as'  =>'invoice'
]);

Route::get('dashboard/manageOrder/pdf/{id}',[
	'uses'=>'Practice@pdf',
	'as'  =>'pdf'
]);

Route::get('dashboard/manageOrder/pdf/{id}',[
	'uses'=>'Practice@pdf',
	'as'  =>'pdf'
]);

Route::get('dashboard/manageOrder/removeOrder/{id}',[
	'uses'=>'Practice@removeOrder',
	'as'  =>'removeOrder'
]);

Route::get('/contact',[
	'uses'=>'Practice@contact',
	'as'  =>'contact'
]);

Route::POST('/contact/sendMail',[
	'uses'=>'Sendmail@index',
	'as'  =>'sendMail'
]);

Route::get('/blog',[
	'uses'=>'blog@Blog',
	'as'  =>'blog'
]);

Route::get('/ajax/{mail}',[
	'uses'=>'AjaxCheck@index',
	'as'  =>'ajax'
]);

Route::get('/adminLogin/{mail}',[
	'uses'=>'AjaxCheck@adminLogin',
	'as'  =>'adminLogin'
]);

Route::get('/log/{mail}',[
	'uses'=>'AjaxCheck@log',
	'as'  =>'log'
]);
