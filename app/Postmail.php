<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postmail extends Model
{
    protected $fillable = ['name','email','subject','message'];
}
