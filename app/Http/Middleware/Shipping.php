<?php

namespace App\Http\Middleware;

use Closure;

class Shipping
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    public function handle($request, Closure $next)
    {
        if(session()->get('shippingId')){
            return $next($request);
        }
        else{
            return redirect('/shipping');
        }
    }


}
