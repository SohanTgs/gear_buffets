<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Product;
use App\Order;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $customers = Customer::count();
        $products = Product::where('publication_status',1)->count();
        $orders = Order::where('order_status','Pending')->count();
        $revenue = Order::where('order_status','Completed')->get();
        return view('admin.home.home',[
           'customers'=>$customers,
           'products'=>$products,
           'orders'=>$orders, 
           'revenue'=>$revenue 

        ]);
    }



}
