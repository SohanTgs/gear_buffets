<?php

namespace App\Http\Controllers;

use App\Category;
use App\Brand;
use App\Product;
use App\Customer;
use Illuminate\Http\Request;
use DB;
use Cart;
use Session;
use Mail;
use App\Shipping;
use App\Order;
use App\OrderDetails;
use App\Payment;
use PDF;
use App\User;
class Practice extends Controller
{


  public function index(){
    $newProduct = Product::where('publication_status',1)
                                ->orderBy('id','Desc')
                                ->take(3)
                                ->get();
    $products =  Product::where('publication_status',1)
                                ->paginate(3);

    	return view('front.home.home',[
        'newProduct'=>$newProduct,
        'products'=>$products
      ]);
  }

  public function product_category(){
    if(isset($_GET['id'])){
        $id = $_GET['id'];
      $product = Product::where('category_id',$id)
                          ->where('publication_status',1)
                          ->get();
    return view('front.category.category',['product'=>$product]);
    }else{
        $allProducts = Product::where('publication_status',1)->paginate(10);
  		return view('front.category.category',['allProducts'=>$allProducts]);
    }
    
  }

  public function details($id){
      $product = Product::find($id);
    //  return $product;
  		return view('front.details_product.details',['product'=>$product]);
  }


  public function add_cetegory(){
  		return view('admin.add_category.add_category');
  }

  public function category_save(Request $request){
    $this->validate($request,[
      'category_name'=>'required|unique:categories',
      'category_description'=>'required',
      'publication_status'=>'required'
    ]);

      $category = new Category();
      $category->category_name = $request->category_name;
      $category->category_description = $request->category_description;
      $category->publication_status = $request->publication_status;
      $category->save();
        return redirect('dashboard/add_cetegory')->with('message','Succesfully category added');
  }

  public function manage_category(){
    $categories = Category::all(); 
    $categories = Category::orderBy('id', 'Desc')->paginate(10);
      return view('admin.manage_category.manage_category',['categories'=>$categories]);
  }

  public function Unpublished_category($id){
      $categories = Category::find($id);
      $categories->publication_status = 0;
      $categories->save();
      return redirect('dashboard/manage_category')->with('sms','publication status unpublished');
   }

  public function published_category($id){
      $categories = Category::find($id);
      $categories->publication_status = 1;
      $categories->save();
      return redirect('dashboard/manage_category')->with('sms2','publication status published');
   }

   public function update_category($id){
    $category = Category::find($id);
    return view('admin.update.update',['category'=>$category]);
   }

  public function update_save(Request $request){
     $category = Category::find($request->category_id);
     $category->category_name = $request->category_name;
     $category->category_description = $request->category_description;
     $category->publication_status = $request->publication_status;
     $category->save();
     return redirect('dashboard/manage_category')->with('message','Update category succesfully');
  }

  public function delete_category($id){
      $category = Category::find($id);
      $category->delete();
      return redirect('dashboard/manage_category')->with('message3','Delete category succesfully');
  }

  public function add_brand(){
    return view('admin.add_brand.add_brand');
  }

  public function manage_brand(){
    $brands = Brand::all();
    $brands = Brand::orderBy('id','Desc')->paginate(10);
    return view('admin.manage_brand.manage_brand',['brands'=>$brands]);
  }

  public function save_brand(Request $request){
    $this->validate($request,[
      'brand_name'=>'required|unique:brands',
      'brand_description'=>'required',
      'publication_status'=>'required'
    ]);
  
    $brand = new Brand();
    $brand->brand_name = $request->brand_name;
    $brand->brand_description = $request->brand_description;
    $brand->publication_status = $request->publication_status;
    $brand->save();
    return redirect('dashboard/add_brand')->with('sms','Succesfully brand added');

  } 

  public function brand_delete($id){
        $brand = Brand::find($id);
        $brand->delete();

        return redirect('dashboard/manage_brand')->with('sms','Delete brand succesfully');
  }
 
  public function Unpublished_brand($id){
          $brand = Brand::find($id);
          $brand->publication_status = 0 ;
          $brand->save();
        return redirect('dashboard/manage_brand')->with('sms2','publication status unpublished');  
  }

  public function Published_brand($id){
          $brand = Brand::find($id);
          $brand->publication_status = 1 ;
          $brand->save();
        return redirect('dashboard/manage_brand')->with('sms3','publication status published');  
  }

  public function update_brand($id){
        $brand = Brand::find($id);
        return view('admin.update_brand.update_brand',['brand'=>$brand]);
  }

  public function brandUpdate_save(Request $request){
       $brand = Brand::find($request->brand_id);
       $brand->brand_name = $request->brand_name;
       $brand->brand_description = $request->brand_description;
       $brand->publication_status = $request->publication_status;
       $brand->save();
       return redirect('dashboard/manage_brand')->with('message','Update brand succesfully');
  }

  public function add_product(){
    $categories = Category::where('publication_status',1)->get();
    $brands = Brand::where('publication_status',1)->get();

      return view('admin.add_product.add_product',[
          'categories'=>$categories,
          'brands'=>$brands,
      ]);
  }

  public function new_product(Request $request){
    $this->validate($request,[
        'category_id'=>'required',
        'brand_id'=>'required',
        'product_name'=>'required',
        'product_price'=>'required',
        'product_quantity'=>'required',
        'short_description'=>'required',
        'long_description'=>'required',
        'product_image'=>'required',
        'publication_status'=>'required'

    ]);

    $productImage = $request->file('product_image');
    $productName = $productImage->getClientOriginalName();
    $directory = 'product-image/';
    $ImageUrl = $directory.$productName;
    $productImage->move($directory,$productName);
     
      $product = new Product(); 
      $product->category_id = $request->category_id;
      $product->brand_id = $request->brand_id;
      $product->product_name = $request->product_name;
      $product->product_price = $request->product_price;
      $product->product_quantity = $request->product_quantity;
      $product->short_description = $request->short_description;
      $product->long_description = $request->long_description;
      $product->product_image = $ImageUrl;
      $product->publication_status = $request->publication_status;
      $product->save();

      return redirect('dashboard/add_product')->with('message','Product info save succesfully');
  }
 
  public function manage_product(){
    $products = DB::table('products')
                          ->join('categories', 'products.category_id', '=', 'categories.id')
                          ->join('brands', 'products.brand_id', '=', 'brands.id')
                          ->select('products.*', 'categories.category_name', 'brands.brand_name')
                          ->orderBy('id','Desc')
                          ->paginate(10);
                         // ->get();
    return view('admin.manage_product.manage_product',['products'=>$products]);
  }

  public function product_delete($id){
        $product = Product::find($id);
        unlink($product->product_image);
        $product->delete();
        return redirect('dashboard/manage_product')->with('sms','Delete product succesfully');
  }


  public function product_unpublished($id){
        $product = Product::find($id);
        $product->publication_status = 0;
        $product->save();
      return redirect('dashboard/manage_product')->with('sms2','Publication status unpublished');   
  }

  public function product_published($id){
        $product = Product::find($id);
        $product->publication_status = 1;
        $product->save();
      return redirect('dashboard/manage_product')->with('sms2','Publication status Published');   
  }

  public function product_update($id){
      $products = Product::find($id);
      $categories = Category::where('publication_status',1)->get();
      $brands = Brand::where('publication_status',1)->get();                     

          return view('admin.product_update.product_update',[
            'products'=>$products,
            'categories'=>$categories,
            'brands'=>$brands

          ]);
  }

  Public function save_productUpdate(Request $request){
        $product = Product::find($request->product_id);
           
      if ($request->hasFile('product_image')) {
        unlink($product->product_image);
           $productImage = $request->file('product_image');
           $productName = $productImage->getClientOriginalName();
           $directory = 'product-image/';
           $ImageUrl = $directory.$productName;
           $productImage->move($directory,$productName);
       $product->product_image = $ImageUrl;
      }

     $product->category_id = $request->category_id;
     $product->brand_id = $request->brand_id;
     $product->product_name = $request->product_name;
     $product->product_price = $request->product_price;
     $product->product_quantity = $request->product_quantity;
     $product->short_description = $request->short_description;
     $product->long_description = $request->long_description;
  
     $product->publication_status = $request->publication_status;
     $product->save();
     
     return redirect('dashboard/manage_product')->with('message','Update product succesfully');
  
  }

  public function addCart(Request $request){
    
      $product = Product::find($request->id);
    
    Cart::add(array(
    'id' => $request->id, 
    'name' => $product->product_name,
    'price' => $product->product_price,
    'quantity' => $request->quantity,
    'attributes' => array(
        'image' => $product->product_image
      ),
));
// return 21;
      return redirect('addCart/cartShow');

  }

  public function cartShow(){
    $cartProduct = Cart::getContent();
      return view('front.cartShow.cartShow',['cartProduct'=>$cartProduct]);
  }

  public function deleteCart($id){
        Cart::remove($id);
        return redirect('addCart/cartShow')->with('message','Update product succesfully');
  }

  public function cartEdit(Request $request){
  
   /* Cart::Update($request->id,[
    'quantity'=>$request->quantity,
    ]);
        return 21; */
   
  }

  public function checkout(){
      return view('front.checkout.checkout');
  }

  public function costomer_registration(Request $request){
      $this->validate($request,[
      'email'=>'required|unique:customers',
      'name'=>'required',
      'number'=>'required',
      'password'=>'required',
      'address'=>'required'
    ]);
      $customer = new Customer();
      $customer->name = $request->name;
      $customer->number = $request->number;
      $customer->email = $request->email;
      $customer->password = bcrypt($request->password);
      $customer->address = $request->address;
      $customer->save();

      $customerId = $customer->id;
      Session::put('customerId',$customerId);
      Session::put('customerName',$customer->name); 
      /* $data = $customer->toArray(); 
       Mail::send('front.mail.mail' ,$data, function($message) as ($data){
          $message->to($data['email']);
          $message->subject('Confirmation mail');
       });*/
       return redirect('checkout/shipping');
  }
    
    public function shipping(){
      $customerId = Customer::find(Session::get('customerId'));
      return view('front.shipping.shipping',['customerId'=>$customerId]);
    }

    public function shippingSave(Request $request){
        $shipping = new Shipping();
        $shipping->name = $request->name;
        $shipping->email = $request->email;
        $shipping->number = $request->number;
        $shipping->address = $request->address;
        $shipping->save();
       
       Session::put('shippingId',$shipping->id);
     
        return redirect('checkout/payment');
    }

    public function payment(){
      return view('front.payment.payment');
    }

    public function order(Request $request){
       
        $paymentType = $request->payment_type;
        
        if($paymentType == 'Cash'){
          $order = new Order();
          $order->customer_id = Session::get('customerId');
          $order->shipping_id = Session::get('shippingId');
          $order->customer_id = Session::get('customerId');
          $order->order_total = Session::get('orderTotal');
          $order->save();

          $payment = new Payment();
          $payment->order_id = $order->id;
          $payment->payment_type = $paymentType;
          $payment->save();

          $cartProducts = Cart::getContent();
          foreach($cartProducts as $cartProduct){
              $orderDetail = new OrderDetails();
              $orderDetail->order_id = $order->id; 
              $orderDetail->product_id = $cartProduct->id; 
              $orderDetail->product_name = $cartProduct->name;
              $orderDetail->product_price = $cartProduct->price;
              $orderDetail->product_quantity = $cartProduct->quantity;
              $orderDetail->save(); 
              }

              Cart::clear();
              return redirect('/completeOrder');
        }
        elseif($paymentType == 'Paypal'){
            echo 'Paypal';
        }
        else{
          echo 'bKash';
        }
    }

    public function completeOrder(){
      return redirect('addCart/cartShow')->with('sms','Completed');
    }

    public function customers_login(Request $request){
        $customer = Customer::where('email',$request->email)->first();
       
        if(password_verify($request->password, $customer->password)){
         Session::put('customerId',$customer->id);
         Session::put('customerName',$customer->name);
         return redirect('checkout/shipping');
        }else{
         return redirect('checkout')->with('message','Invalid password');
        }
    }

    Public function customLogout(){
      Session::forget('customerId');
      Session::forget('customerName');
    return redirect('/');
    }

    public function manageOrder(){
      $orders = DB::table('orders')
                    ->join('customers','orders.customer_id','=','customers.id')
                    ->join('payments','orders.id','=','payments.order_id')
                    ->select('orders.*','customers.name','payments.payment_status','payments.payment_type')
                    ->get();
                   
      return view('admin.manageOrder.manageOrder',['orders'=>$orders]);
    }

    public function viewOrder($id){
      $order = Order::find($id);
      $customer = Customer::find($order->customer_id);
      $shipping = Shipping::find($order->shipping_id);
      $orderDetail = OrderDetails::where('order_id',$order->id)->get();
      $payment = Payment::where('order_id',$order->id)->first();
     
      return view('admin.viewOrder.viewOrder',[
        'customer'=>$customer,
        'shipping'=>$shipping,
        'payment'=>$payment,
        'orderDetail'=>$orderDetail,
        'order'=>$order
      ]);
   
    }

    public function invoice($id){
      $order = Order::find($id);
      $customer = Customer::find($order->customer_id);
      $shipping = Shipping::find($order->shipping_id);
      $orderDetail = OrderDetails::where('order_id',$order->id)->get();
      $payment = Payment::where('order_id',$order->id)->first();
     
      return view('admin.invoice.invoice',[
        'customer'=>$customer,
        'shipping'=>$shipping,
        'payment'=>$payment,
        'orderDetail'=>$orderDetail,
        'order'=>$order
      ]);

    }

    public function pdf(){
        $pdf = PDF::loadView('admin.pdf.pdf');
        return $pdf->stream('invoice.pdf');
    }

    public function removeOrder($id){
        $order = Order::find($id);
        $payment = Payment::find($order->id);
        $detail = OrderDetails::find($order->id);
    
         $order->delete();
         $payment->delete();
         $detail->delete();
         return redirect('dashboard/manageOrder')->with('sms','OrderDetails deleted succesfully');
  }

  public function contact(){
    return view('front.contact.contact');
  }

  public function profile(){
    return view('admin.profile.profile');
  }

    public function settings(){
    return view('admin.settings.settings');
  }

  public function compelteStatus($id){
        $order = Order::find($id);
        $payment = Payment::find($id);
        $order->order_status = 'Completed';
        $payment->payment_status = 'Completed';
        $order->save();
        $payment->save();

        return redirect('dashboard/manageOrder')->with('message','Order and payment status completed succesfully');
  }

  public function profileUpdate(Request $request){
          $user = User::find($request->id);
          $user->name = $request->name;
          $user->email = $request->email;
          $user->mobile = $request->mobile;
          $user->location = $request->location;
          $user->bio = $request->bio;
          $user->save();
          return redirect('dashboard/profile')->with('message','Profle updated succesfully');

}

  public function profilePasswordChange(Request $request){
     $this->validate($request,[
      'password'=>'required',
      'changePassword'=>'required'
     ]);
      $user = User::find($request->id);
      if (password_verify($request->password, $user->password)) {
          $user->password = bcrypt($request->changePassword);
          $user->save();
          return redirect('dashboard/profile')->with('message2','Password changed succesfully');
      }else{
        return redirect('dashboard/profile')->with('message3','Please enter valid password');
      }
  
  }

  public function adminPicture(Request $request){
   $user = User::find($request->id);
   $this->validate($request,[
    'image'=>'required'
   ]);
   
    if($user->image == null){
    $image = $request->file('image');
    $imageName = $image->getClientOriginalName();
    $directory = 'admin-image/';
    $ImageUrl = $directory.$imageName;
    $image->move($directory,$imageName);   

    $user->image = $ImageUrl;
    $user->save();
    return redirect('dashboard/profile');
    }
    else{
    unlink($user->image);
    $image = $request->file('image');
    $imageName = $image->getClientOriginalName();
    $directory = 'admin-image/';
    $ImageUrl = $directory.$imageName;
    $image->move($directory,$imageName);   

    $user->image = $ImageUrl;
    $user->save();
    return redirect('dashboard/profile');
    }

  }





}

