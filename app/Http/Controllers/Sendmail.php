<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Usermail;
use App\Postmail;
class Sendmail extends Controller
{
    public function index(Request $request){
        $this->validate($request,[
    		'message'=>'required',
    		'name'=>'required',
    		'email'=>'required|email',
    		'subject'=>'required',
    	]);

    		$data = array(
    			'name'=>$request->name,
    			'body'=>$request->message,
    			'email'=>$request->email,
    			'subject'=>$request->subject,
    				);
    		Mail::send('front.mail.mail',$data,function ($message) use ($data) {
    		$message->from($data['email'],$data['name'])
    				->to('imsohan000@gmail.com','Gear-buffets')
    				->subject($data['subject']);
                });
          
            $usermail = new Usermail();
            $usermail->name = $request->name;
            $usermail->email = $request->email;
            $usermail->subject = $request->subject;
            $usermail->message = $request->message;
            $usermail->save();

    		return redirect('/contact')->with('message','We have recieved your valueable email');
    }	


    public function InboxMail(){
            $usermail = Usermail::all();
            $count2 = Postmail::count();
            $count1 = Usermail::count();
        return view('admin.InboxMail.InboxMail',['usermail'=>$usermail,'count1'=>$count1,'count2'=>$count2]);
    }

    public function MailDelete($id){
         $usermail = Usermail::find($id);
         $usermail->delete();
         return redirect('/InboxMail')->with('message','Successfully deleted');
    }

    public function SendingMail(Request $request){
       
        $this->validate($request,[
            'message'=>'required',
            'name'=>'required',
            'email'=>'required|email',
            'subject'=>'required',
        ]);

        $data = array(
                'name'=>$request->name,
                'body'=>$request->message,
                'email'=>$request->email,
                'subject'=>$request->subject,
                    );
            Mail::send('admin.mail.mail',$data,function ($message) use ($data) {
            $message->from('imsohan000@gmail.com','Gear-buffets')
                    ->to($data['email'])
                    ->subject($data['subject']);
                });

            $send = new Postmail ();
            $send->name = $request->name;
            $send->email = $request->email;
            $send->subject = $request->subject;
            $send->message = $request->message;
            $send->save();

            return redirect('/InboxMail')->with('message2','Successfully send email');
    }

    public function showSendEmail(){
        $count1 = Postmail::count();
        $count2 = Usermail::count();
        $post = Postmail::all();
        return view('admin.postmail.postmail',['post'=>$post,'count1'=>$count1,'count2'=>$count2]);
    }

    public function postMailDelete($id){
        $mail = Postmail::find($id);
        $mail->delete();

        return redirect('/InboxMail/showSendEmail')->with('message','Successfully deleted email');
    }


    public function compose(){
        return view('admin.compose.compose');
    }








}
