<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usermail extends Model
{
   protected $fillable = ['name','email','subject','message','status'];
}
