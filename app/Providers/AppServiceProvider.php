<?php

namespace App\Providers;
use View;
use Illuminate\Support\ServiceProvider;
use Cart;
use App\Category;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        View::composer('*',function($view){
            $view->with('count',Cart::getTotalQuantity());
        });


        View::composer('front.category.category',function($view){
            $view->with('categories',Category::where('publication_status',1)->get());
        });


    }
}
